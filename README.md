# azure-oat-automation-demo

Prerequisites: </br>

1. **Jenkins** </br>
   a. Install Jenkins </br>
   b. Create a Pipeline job
   c. Jenkins plugins required - sshSteps.
   d. AZ CLI required in the jenkins node (where the job would be executed)

2. **Azure portal** </br>
   a. Create resource group
   b. Create a VM with username and password.
   c. Create a service principle to access the subscription as a contributor.
   d. AZ CLI required in the jenkins node (where the job would be executed)
   

#################################################################################################################

Below are the activities carried out in this job.
 
Verify the list of services and processes running in a VM

Shutdown/restart the VM

Verify the list of services and processes again

Generate a report with list of services before and after the restart.

#################################################################################################################

References - 

https://docs.microsoft.com/en-us/cli/azure/create-an-azure-service-principal-azure-cli

https://docs.microsoft.com/en-us/cli/azure/vm?view=azure-cli-latest

https://docs.microsoft.com/en-us/azure/virtual-machines/ssh-keys-portalsrr

